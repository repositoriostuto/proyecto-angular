import { ProyectoNuevoAngularrPage } from './app.po';

describe('proyecto-nuevo-angularr App', () => {
  let page: ProyectoNuevoAngularrPage;

  beforeEach(() => {
    page = new ProyectoNuevoAngularrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
