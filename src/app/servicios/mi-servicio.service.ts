import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise' 
import { ResponseServicioPaises } from '../modelo/response-servicio-paises';

@Injectable()
export class MiServicioService {
  
  url: string = 'https://restcountries.eu/rest/v2/all';
  private headers = new Headers({'Accept': 'application/json'});


  constructor(private http: Http) { }

  obtenerPaises(): Promise<ResponseServicioPaises[]> {
    let headers = this.headers
    return this.http.get(this.url, {"headers": headers})
    .toPromise()
    .then(response => (response.json() as Array<ResponseServicioPaises>))
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log(JSON.stringify(error));
    return Promise.reject(error.message || error);

  }

}
