import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mi-componente',
  templateUrl: './mi-componente.component.html',
  styleUrls: ['./mi-componente.component.css']
})
export class MiComponenteComponent implements OnInit {

  dato: string = "Creando un Componente de Ejemplo!!"

  constructor() { }

  ngOnInit() {
    console.log('componente inicializado....');
  }
  funcionEjemplo(): string {
    return 'Funcion de ejemplo componente hijo';
  }

}
