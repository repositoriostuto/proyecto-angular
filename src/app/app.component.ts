import { Component } from '@angular/core';
import { MiComponenteComponent} from './mi-componente/mi-componente.component';
import { MiServicioService } from './servicios/mi-servicio.service';
import { ResponseServicioPaises } from './modelo/response-servicio-paises';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[MiComponenteComponent, MiServicioService]
})
export class AppComponent {
  title = 'Hola Mundo!';

  visible = false;
  responsePaises: ResponseServicioPaises[];

  decirAdios(){
    this.visible = true ;
  }
  volverAtras(){
    this.visible = false;
  }

  constructor(private miComponenteComponent: MiComponenteComponent,
    private miServicioServicie: MiServicioService) {
    
  }
  probarFuncionComponenteHijo(){
    alert(this.miComponenteComponent.funcionEjemplo());
  }

  obtenerPaises(){
    this.miServicioServicie.obtenerPaises().then(
      response => {this.responsePaises = response}
    )
  }

}
